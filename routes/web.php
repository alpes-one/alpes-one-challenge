<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/brands', 'CrawlerController@listBrands');
Route::get('/api/list-vehicles/{brand}', 'CrawlerController@getVehicles');
Route::get('/api/vehicle/{url}', 'CrawlerController@getVehicle');

Route::get('/api-test', 'CatalogController@listBrands');
Route::get('/list-vehicles/{name}', 'CatalogController@listVehicles');
Route::get('/vehicle/{name}', 'CatalogController@getVehicle');
