<?php
  
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use Goutte\Client;
  
class CrawlerController extends Controller
{
    function listBrands()
    {
        // List of the basic brands
        $brands = [
            ["slug"=>"audi", "name"=>"Audi"],
            ["slug"=>"bmw", "name"=>"BMW"],
            ["slug"=>"chevrolet", "name"=>"Chevrolet"],
            ["slug"=>"citroen", "name"=>"Citroen"],
            ["slug"=>"fiat", "name"=>"Fiat"],
            ["slug"=>"ford", "name"=>"Ford"],
            ["slug"=>"hyundai", "name"=>"Hyundai"],
            ["slug"=>"honda", "name"=>"Honda"],
            ["slug"=>"jeep", "name"=>"Jeep"],
            ["slug"=>"mercedes-benz", "name"=>"Mercedes Benz"],
            ["slug"=>"mitsubishi", "name"=>"Mitsubishi"],
            ["slug"=>"nissan", "name"=>"Nissan"],
            ["slug"=>"peugeot", "name"=>"Peugeot"],
            ["slug"=>"renaut", "name"=>"Renault"],
            ["slug"=>"toyota", "name"=>"Toyota"],
            ["slug"=>"volkswagen", "name"=>"Volkswagen"],
        ];

        return response(view('api.cars.brands',array('brands'=>$brands)),200, ['Content-TYpe' => 'application/json']);
    }
    
    function getVehicles($brand)
    {
        //These arrays will store the information for each element found by the scrapper
        $name           = array();
        $description    = array();
        $detail         = array();
        $bonus          = array();
        $value          = array();
        $img            = array();
        $href           = array();
        $location       = array();
        $seller         = array();

        // This specific array will reunite all the scrapped data in a single object to deliver the json
        $data           = []; 

        // Initialize scrapping client
        $client = new Client();

        // Getting all registers according with the car brand selected and getting the first 50 registers
        $crawler = $client->request('GET', config('services.crawler.target').'/carro/'.$brand.'?ordenarPor=0&registrosPagina=50');
        
        //Craw Image
        $img = $crawler->filter('.anuncio-thumb-new img')->each(function ($node) {
            return $src = $node->extract(array('src'));
        });

        //Craw Title
        $name = $crawler->filter('.card-header h4')->each(function ($node) {
               return $node->text();
        });

        //Craw Description
        $description = $crawler->filter('.card-description')->each(function ($node) {
            return $node->text();
        });

        //Craw Details
        $detail = $crawler->filter('.card-detalhes')->each(function ($node) {
            return $node->text();
        });

        //Craw Bonus
        $bonus = $crawler->filter('.card-acessorios')->each(function ($node) {
            return $node->text();
        });

        //Craw Value
        $value = $crawler->filter('.value h4')->each(function ($node) {
            return $node->text();
        });

        //Craw Href
        $href = $crawler->filter('.value > a')->each(function ($node) {
            return $href = $node->extract(array('href'));
        });

        //Craw Location
        $location = $crawler->filter('.localizacao span')->each(function ($node) {
            return $node->text();
        });

        //Craw Seller
        $seller = $crawler->filter('.value > a')->each(function ($node) {
            return $node->text();
        });

        // Building the array object in a loop to deliver json
        for($i = 0; $i < sizeof($name); $i++) {
            array_push( $data, [
                'name'          => $name[$i],
                'description'   => $description[$i],
                'img'           => $img[$i],
                'detail'        => $detail[$i],
                'bonus'         => $bonus[$i],
                'value'         => $value[$i],
                'location'      => $location[$i],
                'seller'        => $seller[$i],
                
                // On href we need to use this small trick to remove the %2F from the url.
                // Some urls on seminovos.com.br have a backslash encoded on url, generating a error
                'href'          => preg_replace('/[^a-zA-Z0-9_ -]/s','',implode($href[$i])),
            ]);
        }
        
        // Adding a object name on array
        $json_product = ['cars' => $data];

        // Returns a view that will deliver the result of the scrapping on json format to be consumed for other applications
        return response(view('api.cars.list',array('products'=>$json_product)),200, ['Content-TYpe' => 'application/json']);
    }

    public function getVehicle($url)
    {
        //These arrays will store the information for each element found by the scrapper
        $name           = array();
        $description    = array();
        $price          = array();
        $specs          = array();
        $bonus          = array();
        $detail         = array();
        $imgs           = array();

        // This specific array will reunite all the scrapped data in a single object to deliver the json
        $data = [];

        // Initialize scrapping client
        $client = new Client();
        $crawler = $client->request('GET', config('services.crawler.target').$url);
        
        //Craw the name of car
        $name = $crawler->filter('.item-info h1')->each(function ($node) {
            return $node->text();
        });

        //Craw description
        $description = $crawler->filter('.desc')->each(function ($node) {
            return $node->text();
        });

        //Craw price
        $price = $crawler->filter('.price')->each(function ($node) {
            return $node->text();
        });

        //Craw specs of car
        $specs = $crawler->filter('.row-print')->each(function ($node) {
            return $node->text();
        });

        //Craw all the accessories present on the car
        $bonus = $crawler->filter('.list-styled')->each(function ($node) {
            return $node->text();
        });

        //Craw details about the car
        $detail = $crawler->filter('.meta-properties .description-print')->each(function ($node) {
            return $node->text();
        });

        //Craw Images 
        $imgs = $crawler->filter('.available')->each(function ($node) {
            return $src = $node->extract(array('data-src'));
        });

            // Building the array object in a loop to deliver json
            array_push( $data, [
                'name'          => implode($name),
                'description'   => implode($description),
                'price'         => implode($price),
                'specs'         => implode($specs),
                'bonus'         => $bonus,
                'detail'        => implode($detail),
                'imgs'          => $imgs,
            ]);
        
            $json_product = ['car' => $data];
        // Returns a view that will deliver the result of the scrapping on json format to be consumed for other applications
        return response(view('api.cars.detail',array('products'=>$json_product)),200, ['Content-TYpe' => 'application/json']);
        
    }

}