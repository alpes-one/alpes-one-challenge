<?php
  
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use GuzzleHttp;
use App\API;
  
class CatalogController extends Controller
{
    public function listBrands()
    {
        $brands = API::get( 'api/brands', 'php' );
        return view('cars.brands')->with(['brands'=>$brands]);
    }

    public function listVehicles($name)
    {
        $result = API::get( 'api/list-vehicles/'.$name, 'php' );
        $cars = $result->cars;
        return view('cars.list')->with(['cars'=>$cars]);
    }

    public function getVehicle($name)
    {
        $result = API::get( 'api/vehicle/'.$name, 'php' );
        $car = $result->car[0];
        return view('cars.detail')->with(['car'=>$car]);
    }
}