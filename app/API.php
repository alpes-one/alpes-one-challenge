<?php

namespace App;

use GuzzleHttp\Client; 
use GuzzleHttp\Exception\RequestException;
use Exception;

class API
{   
    //Init Guzzle Client with api jira settings
    public static function client()
    {
        return new Client([
            'base_uri' => config('services.crawler.consumer'),
            'verify' => false
        ]);
    }

    //Export response format
    public static function returnResponse($data, $output)
    {
        //Check Output - 'json' (Output raw json string) 'php' (Output a php array)
        if( $output == 'json' ) 
        {
            return $data;
        }
        elseif( $output == 'php' )
        {
            return json_decode($data);
        }

        return false;
    }

    public static function get( $url, $output )
    {
        try 
        {
            //Init Guzzle
            $http = API::client();
            //Make API Call
            $response = $http->request('GET', $url); 
            $body     = $response->getBody();
            
            return API::returnResponse($body, $output);
        }
        catch (RequestException $e) 
        {   
            \Log::error( $e->getMessage() );
            return false;
        }
    }

    public static function post( $url, $json, $output )
    {
        try 
        {
            //Init Guzzle
            $http = API::client();
            //Make API Call
            $response = $http->request('POST', $url, [
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'json'          => json_decode($json)
            ]); 
            $body = $response->getBody();

            return API::returnResponse($body, $output);
        }
        catch (RequestException $e) 
        {   
            \Log::error( $e->getMessage() );
            return false;
        }
    }

    public static function put( $url, $json, $output )
    {
        try 
        {
            //Init Guzzle
            $http = API::client();

            //Make API Call
            $response = $http->request('PUT', $url, [
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'json'          => json_decode($json)
            ]); 
            $body = $response->getBody();

            return API::returnResponse($body, $output);
        }
        catch (RequestException $e) 
        {   
            \Log::error( $e->getMessage() );
            return false;
        }
    }
}