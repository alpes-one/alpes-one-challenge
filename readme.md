## About AlpesOne Web Crawler

This crawler was designed to get data from the website seminovos.com.br. 

The framework base used here are Laravel 

## Pre requisites

The machine where this application runs needs to have:

- PHP 7+
- Apache 2
- Composer

## How to use

You can simply access the this page: [AlpesOne Web Cralwer](http://alpesone.jsilmarovi.me/) or you can get the code from this repo and execute the following steps:

- After checkout the repo on your local machine, you need to execute the command `composer update`
- Rename the env.example file to .env
- Edit the .env file and change the following variable `CRAWLER_API_CONSUMER=<your env url>`. This variable is used on the API test page
- After perform these steps you can open the console and execute the following command inside the project folder `php artisan serve`. This will generate a server instance to execute the application.
- Keep the terminal running and on browser, type: `http://127.0.0.1:8000` and *voilá*, you are in.