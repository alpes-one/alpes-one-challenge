@extends('layout.app') 
@section('title', 'AlpesOne API Reference') 
@section('content')

<div class="container mb-5">

<div class="row">
    <div class="col-md-3">
        <div class="list-group">
            <a href="#list-brands" class="list-group-item list-group-item-action">List Brands</a>
            <a href="#list-vehicles" class="list-group-item list-group-item-action">List Vehicles by Brand</a>
            <a href="#get-vehicle" class="list-group-item list-group-item-action">Get Vehicle Details</a>
            
          </div>
    </div>

    <div class="col-md-9">
        <h2>API Reference</h2>
        <p>This api contains 3 basic endpoints. Once these endpoints are invoked, a real time crawler is executed on the seminovos.com.br website
            and brings the results on JSON format to be consumed for any application</p>
            <hr>
            <a name="list-brands"></a>
            <h3>List Brands</h3>
            <br>
            <b>Description</b><br>
            <p>This endpoint returns the list of brands</p><br>
            <b>URL Request</b>
            <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">GET</div>
                </div>
                <input type="text" class="form-control" disabled="disabled" value="{{config('services.crawler.consumer')}}/api/brands">
              </div><br>
            <b>Parameters</b><br><br>
            Not Applicable
    
    
            <div class="alert alert-secondary" role="alert">
            <code>
                <pre>
[{
    "slug": "audi",
    "name": "Audi"
}, {
    "slug": "bmw",
    "name": "BMW"
}, {
    "slug": "chevloret",
    "name": "Chevrolet"
}, {
    "slug": "citroen",
    "name": "Citroen"
}, {
    "slug": "fiat",
    "name": "Fiat"
}, {
    "slug": "ford",
    "name": "Ford"
}, {
    "slug": "hyundai",
    "name": "Hyundai"
}, {
    "slug": "honda",
    "name": "Honda"
}, {
    "slug": "jeep",
    "name": "Jeep"
}, {
    "slug": "mercedes-benz",
    "name": "Mercedes Benz"
}, {
    "slug": "mitsubishi",
    "name": "Mitsubishi"
}, {
    "slug": "nissan",
    "name": "Nissan"
}, {
    "slug": "peugeot",
    "name": "Peugeot"
}, {
    "slug": "renaut",
    "name": "Renault"
}, {
    "slug": "toyota",
    "name": "Toyota"
}, {
    "slug": "volkswagen",
    "name": "Volkswagen"
}]
                </pre>
            </code>
            </div>
    
            <p>A typical response is made up of a JSON array composed of the following fields:</p>
    
            <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Field</th>
                    <th scope="col">Type</th>
                    <th scope="col">Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">name</th>
                    <td>string</td>
                    <td>Name of the brand</td>
                  </tr>
                  <tr>
                    <th scope="row">slug</th>
                    <td>string</td>
                    <td>Slug of the brand to access</td>
                  </tr>
                </tbody>
              </table>
    









        <hr>
        <a name="list-vehicles"></a>
        <h3>List Vehicles by Brand</h3>
        <br>
        <b>Description</b><br>
        <p>This endpoint returns a list of 50 vehicles according with the selected brand</p><br>
        <b>URL Request</b>
        <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">GET</div>
            </div>
            <input type="text" class="form-control" disabled="disabled" value="{{config('services.crawler.consumer')}}/api/list-vehicles/{brand}">
          </div><br>
        <b>Parameters</b><br><br>
        <ul>
            <li><b>brand</b>: required, string, the car brand to be returned. Eg: audi, bmw, etc.
        </ul>


        <div class="alert alert-secondary" role="alert">
        <code>
            <pre>
{
    "cars": [{
        "name": "Alfa Romeo 156",
        "description": "2.0 16v TS + Teto Solar",
        "img": ["https:\/\/tcarros.seminovosbh.com.br\/mini_alfa_romeo\/156\/2000\/2001\/2595085\/742378c9067047ab3070f3b131dabc2d6670"],
        "detail": "2000\/2001 166.500km Gasolina Manual",
        "bonus": "\u2022ABS \u2022AIR BAGS 4 \u2022ALARME \u2022AR CONDICIONADO \u2022AR QUENTE \u2022BANCOS DE COURO \u2022C\u00c2MBIO MANUAL \u2022C\u00c2MERA DE R\u00c9",
        "value": "R$ 24.500,00",
        "href": "alfa-romeo-156-ts--teto-solar-20-16v-4portas-2000-2001--2595085"
    }, {
        "name": "Alfa Romeo 164",
        "description": "Super V6 24V",
        "img": ["https:\/\/tcarros.seminovosbh.com.br\/mini_alfa_romeo\/164\/1995\/1995\/2726043\/722aab5c3053e20f9fe7afce8638e83fb46"],
        "detail": "1995\/1995 152.000km Gasolina Manual",
        "bonus": "\u2022ABS \u2022AIR BAGS 2 \u2022AR CONDICIONADO \u2022AR QUENTE \u2022BANCOS DE COURO \u2022C\u00c2MBIO MANUAL \u2022CD \/ MP3 \u2022COMPUTADOR DE BORDO",
        "value": "R$ 25.000,00",
        "href": "alfa-romeo-164-super-v6-24v-30-24v-4portas-1995--2726043"
    }]
}
            </pre>
        </code>
        </div>

        <p>A typical response is made up of a JSON array composed of the following fields:</p>

        <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Field</th>
                <th scope="col">Type</th>
                <th scope="col">Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">name</th>
                <td>string</td>
                <td>Name of the vehicle</td>
              </tr>
              <tr>
                <th scope="row">description</th>
                <td>string</td>
                <td>Small description about the vehicle</td>
              </tr>
              <tr>
                <th scope="row">img</th>
                <td>string</td>
                <td>The url of the highlight image</td>
              </tr>
              <tr>
                <th scope="row">detail</th>
                <td>string</td>
                <td>Some details about the vehicle like fuel type, year of release and others</td>
              </tr>
              <tr>
                <th scope="row">bonus</th>
                <td>string</td>
                <td>Some of the acessories that comes with the vehicle</td>
              </tr>
              <tr>
                <th scope="row">value</th>
                <td>string</td>
                <td>The price of the car</td>
              </tr>
              <tr>
                <th scope="row">location</th>
                <td>string</td>
                <td>Where the car is located</td>
              </tr>
              <tr>
                <th scope="row">seller</th>
                <td>string</td>
                <td>The name of the seller</td>
              </tr>
              <tr>
                <th scope="row">href</th>
                <td>string</td>
                <td>The link for the specific page of the car</td>
              </tr>
            </tbody>
          </table>
      





          <hr>
          <a name="get-vehicle"></a>
          <h3>Get Vehicle Details</h3>
          <br>
          <b>Description</b><br>
          <p>This endpoint will deliver the details of the selected vehicle</p><br>
          <b>URL Request</b>
          <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">GET</div>
              </div>
              <input type="text" class="form-control" disabled="disabled" value="{{config('services.crawler.consumer')}}/api/vehicle/{url}">
            </div><br>
          <b>Parameters</b><br><br>
          <ul>
              <li><b>url</b>: required, string, the url of the vehicle. Eg: nissan-sentra-2.02F-2.0-flex-fuel-16v-mec.-2.0-16v-4portas-2011-2012--2641784
          </ul>
  
  
          <div class="alert alert-secondary" role="alert">
          <code>
              <pre>
{
    "car": [{
        "name": "Nissan Sentra",
        "description": "2.0\/ 2.0 Flex Fuel 16V Mec.",
        "price": "R$ 27.900,00",
        "specs": "Ano\/Modelo 2011\/2012 Quilometragem 149.000 Km C\u00e2mbio Manual Portas 4 Combust\u00edvel Bi-Combust\u00edvel Cor Preto Placa NWN Troca? Aceito Troca",
        "bonus": ["ABS AIR BAGS 2 ALARME AR CONDICIONADO", "AR QUENTE C\u00c2MBIO MANUAL CD \/ MP3 COMPUTADOR DE BORDO", "DESEMBA\u00c7ADOR DIRE\u00c7\u00c3O HIDR\u00c1ULICA FAR\u00d3IS AUXILIARES RETROVISORES EL\u00c9TRICOS", "SENSOR DE ESTACIONAMENTO TRAVAS EL\u00c9TRICAS VIDROS EL\u00c9TRICOS VOLANTE AJUST\u00c1VEL"],
        "detail": "O Propriet\u00e1rio n\u00e3o forneceu observa\u00e7\u00f5es.",
        "imgs": [
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-7356b61dd08efeb0a93408201c4ef89cdbf9.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-45153f0bb904c769ae0ffec90106d9a238a3.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-81089464f638128a7933f1f6adc96ffd823e.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-252605b38c2b414a2a988d1043bbe75af1b.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-379142c5eacddbd38e9c1f1057376ead3ad0.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-99028670aafd3ef36dc0ca504edd18b2c45.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-8181327597cf33f43ce38eae364f12444f7d.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-6762d923dd86d4dbdd5a1acfa5df8e161065.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-89310235ad6e376295cc578c099086985877.jpg"],
            ["https:\/\/carros.seminovosbh.com.br\/nissan-sentra-2011-2012-2641784-75247cbcf77fed0162664955a7192c6b5f8f.jpg"]
        ]
    }]
}
              </pre>
          </code>
          </div>
  
          <p>A typical response is made up of a JSON array composed of the following fields:</p>
  
          <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Field</th>
                  <th scope="col">Type</th>
                  <th scope="col">Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">name</th>
                  <td>string</td>
                  <td>Name of the vehicle</td>
                </tr>
                <tr>
                  <th scope="row">description</th>
                  <td>string</td>
                  <td>Small description about the vehicle</td>
                </tr>
                <tr>
                  <th scope="row">price</th>
                  <td>string</td>
                  <td>The price of the car</td>
                </tr>
                <tr>
                  <th scope="row">specs</th>
                  <td>string</td>
                  <td>The specifications about the vehicle</td>
                </tr>
                <tr>
                  <th scope="row">bonus</th>
                  <td>array</td>
                  <td>List of the acessories that comes with the vehicle</td>
                </tr>
                <tr>
                  <th scope="row">detail</th>
                  <td>string</td>
                  <td>More details about the vehicle</td>
                </tr>
                <tr>
                  <th scope="row">imgs</th>
                  <td>array</td>
                  <td>The urls of images from vehicle gallery</td>
                </tr>
              </tbody>
            </table>
    </div>
</div>





</div>

@endsection