<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>@yield('title') - AlpesOne Crawler</title>
  <meta http-equiv='X-UA-Compatible' content='IE=edge' />
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <meta name="description" content="AlpesOne Crawler - Webcrawler to get data from partner" />
  <meta property="og:title" content="@yield('title') - AlpesOne Crawler" />
  <meta property="og:site_name" content="AlpesOne Crawler" />
  <meta property="og:description" content="AlpesOne Crawler - Webcrawler to get data from partner" />
  <meta property="og:image:width" content="" />
  <meta property="og:image:height" content="" />
  <meta property="og:type" content="website" />
  <meta property="twitter:title" content="@yield('title') - AlpesOne Crawler" />
  <meta property="twitter:description" content="AlpesOne Crawler - Webcrawler to get data from partner" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/d6f1e29daf.js" crossorigin="anonymous"></script>
  </head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/api-test">API Test</a>
      </li>
    </ul>
  </div>
</nav>


<div class="jumbotron">
    <div class="container">
    <h1 class="display-4">AlpesOne Web Crawler</h1>
    <p class="lead">Documentation for the AlpesOne Web Crawler API.</p>
    </div>
</div>
@yield('content')

<div class="jumbotron text-center bg-dark text-white">
  Copyright &copy; 2020 - José Silmarovi Jr. - All rights reserved
</div>
</body>
