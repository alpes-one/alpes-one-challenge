@extends('layout.app') @section('title', 'Car Details') @section('content')

<div class="container mb-5">
<h1>{{$car->name}}</h1>
{{$car->description}}<br>
{{$car->price}}<br>
<hr>
<h4>Especificações:</h4>
    {{$car->specs}}
<hr>

<h4>Acessórios:</h4>
@foreach($car->bonus as $bonus)
    {{$bonus}}
@endforeach
<hr>
<h4>Descrição:</h4>
{!! $car->detail !!}
<hr>
<h4>Imagens:</h4>
@foreach($car->imgs as $img)
    <img src="{{$img[0]}}" style="width: 130px;">
@endforeach

</div>
@endsection