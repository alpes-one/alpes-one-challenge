@extends('layout.app') @section('title', 'List Brands') @section('content')

<div class="container mb-5">
    <h1>API Test</h1>

    <p>This page have the purpose to test the responses of the developed API endpoints. On this page we have a list of cars brands and the result of the first endpoint.</p>
    <p>If you click on some of the brands below, you will be conducted to the list of cars and to the second endpoint</p>

    @foreach($brands as $brand)
    <a href="/list-vehicles/{{$brand->slug}}"><span style="font-size: 17px;" class="badge badge-primary mb-2">{{$brand->name}}</span></a>
        
    @endforeach
</div>
@endsection