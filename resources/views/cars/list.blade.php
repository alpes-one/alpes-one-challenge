@extends('layout.app') 
@section('title', 'List Vehicles') 
@section('content')

<div class="container mb-5">
  @foreach ($cars as $car)
  <a style="color: #000; text-decoration: none" href="/vehicle/{{$car->href}}"><div class="card mb-3">
    <div class="row">
      <div class="col-md-3">
        <img src="{{$car->img[0]}}" class="card-img-top" />
      </div>
      <div class="col-md-6">
        <div class="card-body">
          <h5 class="card-title">{{$car->name}}</h5>
          <p>{{ $car->description }}</p>
          @foreach(explode(' ', $car->detail) as $info)
          <span class="badge badge-info">{{ $info }}</span>
          @endforeach
          <br />
          <small>{{$car->bonus}}</small>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card-body text-center">
        <h4>{{$car->value}}</h4>
        <br>{{$car->location}}
        <br>{{$car->seller}}
    </div>
      </div>
    </div>
  </div>
</a>

  @endforeach
</div>
@endsection